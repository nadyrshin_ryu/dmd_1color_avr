//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <timers.h>
#include <dmd_demo.h>
#include <disp1color.h>
#include <dmd_1color.h>
#include <font.h>

// �������� ������ ����������
uint16_t AlgStep, AlgStep2, AlgStep3;
uint8_t TestBuff[16];
int16_t X, Y;


//==============================================================================
// ������� �������� ����� ������. ���������� ���-�� ������ � ����� �������.
// �� ��������� pPeriod ���������� ������ � �� ����� �������
//==============================================================================
uint16_t screen_start(uint8_t ScreenNum, uint16_t *pPeriod)
{
  AlgStep = AlgStep2 = AlgStep3 = 0;
  
  switch (ScreenNum)
  {
  // ���������� �������� ������ � ��������� �������� �������
  case 0:
    tmr2_stop();
    *pPeriod = 30;
    return 128; 
  // ���������� �������� ������ � ��������� �������� �������
  case 1:
    tmr2_stop();
    *pPeriod = 30;
    return 128; 
  // ������� ������ �������
  case 2:
    tmr2_start();
    *pPeriod = 50;
    X = 32;
    disp1color_FillScreenbuff(0);       // ������� ������
    disp1color_UpdateFromBuff();
    return 280; 
  // ������ ����������� ����� 
  case 3:
    *pPeriod = 10;
    disp1color_FillScreenbuff(0);       // ������� ������
    disp1color_UpdateFromBuff();
    return 1000; 
  // ������� ������
  case 4:
    *pPeriod = 70;
    X = 32;
    disp1color_FillScreenbuff(0);       // ������� ������
    disp1color_UpdateFromBuff();
    return 150; 
  // ������� ������ ���� ������
  case 5:
    *pPeriod = 100;
    Y = -10;
    disp1color_FillScreenbuff(0);       // ������� ������
    disp1color_UpdateFromBuff();
    return 50; 
  default:
    *pPeriod = 20;
    disp1color_FillScreenbuff(0);       // ������� ������
    disp1color_UpdateFromBuff();
    return 64;
  }
}
//==============================================================================


//==============================================================================
// ���������, ����������� ������ � ������� EffectNum. ������ ���������� � �������� ��������
//==============================================================================
void screen_tick(uint8_t ScreenNum)
{
  switch (ScreenNum)
  {
  // ���������� �������� ������ � ��������� �������� �������
  case 0:
    AlgStep2 = AlgStep >> 3;
    AlgStep3 = AlgStep & 0x07;
    TestBuff[15 - AlgStep2] ^= (1 << AlgStep3);
    DMD_1COLOR_SendToScreen(0, TestBuff, sizeof(TestBuff));
    AlgStep++;
    break;
  // ���������� �������� ������ � ��������� �������� �������
  case 1:
    AlgStep2 = AlgStep >> 3;
    AlgStep3 = AlgStep & 0x07;
    TestBuff[15 - AlgStep2] ^= (1 << AlgStep3);
    DMD_1COLOR_SendToScreen(0, TestBuff, sizeof(TestBuff));
    AlgStep++;
    break;
  // ������� ������
  case 2:
    disp1color_FillScreenbuff(0);       // ������� ������
    disp1color_printf(X--, 0, FONTID_10X16F, "����������� � ��������� %d", 2016);
    disp1color_UpdateFromBuff();
    break;
  // ������ ����������� �����
  case 3:
    disp1color_FillScreenbuff(0);       // ������� ������
    disp1color_DrawRectangle(0, 0, 31, 15);
    disp1color_printf(5, 5, FONTID_6X8M, "%d", AlgStep++);
    disp1color_UpdateFromBuff();
    break;
  // ������� ������
  case 4:
    disp1color_FillScreenbuff(0);       // ������� ������
    disp1color_printf(X--, 4, FONTID_6X8M, "������� ������ %d", AlgStep++);
    disp1color_UpdateFromBuff();
    break;
  // ������� ������ ���� ������
  case 5:
    disp1color_FillScreenbuff(0);       // ������� ������
    disp1color_printf(0, Y++, FONTID_6X8M, "� ���");
    disp1color_UpdateFromBuff();
    break;
  default:
    break;
  }
}
//==============================================================================
